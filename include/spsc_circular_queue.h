/////////////////////////////////////////////////////////////////////////////////////////
//
// Single-Producer Single-Consumer (SPSC) nonblocking fixed-size circular queue
// Works with pointers to objects. Based on FastForward algorithm
// The main use case is to transfer ownership of a data from one thread to another without
// using locks to prevent thread contention.
// Some CPU architectures may require memory barrier in push()
/////////////////////////////////////////////////////////////////////////////////////////
#ifndef SPSC_CIRCULAR_QUEUE_H
#define SPSC_CIRCULAR_QUEUE_H

#include <cstdint>
#include <stddef.h>

namespace p42
{
    template <typename Element, int mSize = 512> // Queue size. Should be power of 2
    requires (mSize != 0) && ((mSize & (mSize - 1)) == 0) // same as std::has_single_bit
    class spsc_circular_queue
    {
    private:
        // Ensure that enqueue and dequeue operate on different cash lines
        // to prevent cache trashing by two different threads
        // 64 bytes is the common size of a CPU cache line, but M1 got 128
        static const int mCacheLineSize = 128; 
        volatile uint32_t mHead; // index for Consumer's read operations
        uint32_t mPadding1[mCacheLineSize / sizeof(uint32_t) - 1]; 

        volatile uint32_t mTail; // index for Producer's write operations
        uint32_t mPadding2[mCacheLineSize / sizeof(uint32_t) - 1]; 

        Element* mQueue[mSize];
        
    public:
        spsc_circular_queue(): mHead(0), mTail(0), mQueue{} {};

        // prevent from copying
        spsc_circular_queue(const spsc_circular_queue&) = delete;
        void operator=(spsc_circular_queue&) = delete;

        // Producer's interface
        
        // true if there are no free space in the queue
        bool is_full() const
        {
            return (mQueue[mTail] != nullptr);
        }

        // writes element's pointer to the end of the queue
        bool push(Element* const pElement)
        {
            
            if (is_full() || pElement == nullptr)
                return false;

            // Memory barrier at this point if CPU architecture requires it

            mQueue[mTail] = pElement;

            mTail += 1;
            mTail &= mSize - 1; // zero if mTail >= mSize
            return true;
        }

        // Consumer's interface

        // true if the queue is empty
        bool is_empty() const
        {
            return (mQueue[mHead] == nullptr);
        }

        // removes and returns element from the queue, 
        // returns nullptr if the queue is empty
        Element* pop()
        {
            Element* ret = nullptr;
            if (!is_empty()) {
                ret = mQueue[mHead];
                mQueue[mHead] = nullptr;
                mHead += 1;
                mHead &= mSize - 1; // zero if mHead >= mSize
            }
            return ret;
        }

        // returns element from the queue without removing it, 
        // returns nullptr if the queue is empty
        Element* front() const
        {
            return mQueue[mHead];
        }
    };
} // namespace p42

#endif //SPSC_CIRCULAR_QUEUE_H