/////////////////////////////////////////////////////////////////////////////////////////
// Red-black binary tree with parent pointers. 
//
// prb_tree_node size is 3 pointers: parent, left child, right child
// Node address must be aligned by 2 bytes as it uses LSB as red/black flag
// prb_tree_node has no std:: dependencies. If std is supported, use wrapper prb_tree
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef PRB_TREE_NODE_H
#define PRB_TREE_NODE_H

#include <stddef.h>

namespace p42
{
    struct prb_tree_node
    {
        using EraseFunc = void (*)(prb_tree_node*);

        prb_tree_node *mParent; // LSB used as red\black flag. 1 is red, 0 is black. 
                                // node location must be aligned by 2 bytes
        prb_tree_node *mChild[2];

        prb_tree_node* left()
        { 
            return mChild[0];
        }

        prb_tree_node* right()
        {
            return mChild[1];
        }

        prb_tree_node* parent()
        { 
            return (prb_tree_node*)(((size_t)mParent)&(~((size_t)1))); // reset LSB
        } 
#if defined(__GNUC__) && !defined(__clang__) // gcc
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wuninitialized"
    #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
        bool is_red()
        { 
            return ((size_t)mParent)&((size_t)1); // true if LSB is set - the Node is red
        }
#if defined(__GNUC__) && !defined(__clang__)
    #pragma GCC diagnostic pop
#endif
        void set_red()
        { 
            mParent = (prb_tree_node*)(((size_t)mParent)|((size_t)1)); // set LCB
        }

        void set_black()
        { 
            mParent = (prb_tree_node*)(((size_t)mParent)&(~((size_t)1))); // reset LCB
        }

        void set_parent(prb_tree_node *pParent)
        {
            bool wasRed = is_red();
            mParent = pParent;
            if (wasRed) set_red();
        }


        // static
        static bool is_red(prb_tree_node* pNode)
        {
            return pNode != nullptr && pNode->is_red();
        } 

        static prb_tree_node* rotate_once(prb_tree_node* pRoot, int pDirection);
        static prb_tree_node* rotate_twice(prb_tree_node* pRoot, int pDirection);
        static void remove(prb_tree_node **pRoot, prb_tree_node* pNode);

        // Key compare function. Returns node child's index: 0 if key1 < key2, 1 if key1 > key 2, -1 if key1 == key2
        template<typename KeyT> 
        static int key_compare(const KeyT& pKey1, const KeyT& pKey2)
        {
            if (pKey1 < pKey2)
                return 0;
            if (pKey2 < pKey1)
                return 1;
            return -1;
        }

        template<typename KeyT>
        static prb_tree_node* insert(prb_tree_node **pRoot, prb_tree_node* pNode, ptrdiff_t pKeyOffset);
        
        template<typename KeyT>
        static prb_tree_node* remove(prb_tree_node **pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset);

        template<typename KeyT>
        static prb_tree_node* find(prb_tree_node *pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset);
        
        template<typename KeyT>
        static prb_tree_node* find_eq_or_greater(prb_tree_node *pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset);
        
        static prb_tree_node* next(prb_tree_node *pRoot, prb_tree_node* pCurrNode = nullptr);
        
        template<EraseFunc erase_func>
        static void erase(prb_tree_node *pRoot);
    };

    inline prb_tree_node* prb_tree_node::rotate_once(prb_tree_node* pRoot, int pDirection)
    {
        //save opposite child
        prb_tree_node* temp = pRoot->mChild[!pDirection];
        //rotate
        prb_tree_node* temp2 = temp->mChild[pDirection];
        pRoot->mChild[!pDirection] = temp2;
        if (temp2)
            temp->mChild[pDirection]->set_parent(pRoot);
        temp->mChild[pDirection] = pRoot;

        //save root's parent
        prb_tree_node* tempParent = pRoot->parent();
        //change parent
        pRoot->set_parent(temp); 
        //change color
        pRoot->set_red();
        temp->set_parent(tempParent);
        //change color
        temp->set_black();

        return temp;
    }

    inline prb_tree_node* prb_tree_node::rotate_twice(prb_tree_node* pRoot, int pDirection)
    {
        pRoot->mChild[!pDirection] = rotate_once(pRoot->mChild[!pDirection], !pDirection);
        return rotate_once(pRoot, pDirection);
    }

    // Bottom-up insertion. Returns nullptr if inserted or pointer to the existing node with the same key
    template<typename keyT>
    prb_tree_node* prb_tree_node::insert(prb_tree_node **pRoot, prb_tree_node* pNode, ptrdiff_t pKeyOffset)
    {
        if (*pRoot == nullptr) {
            pNode->mChild[0] = pNode->mChild[1] = nullptr;
            pNode->set_parent(nullptr);
            *pRoot = pNode;
        } else {
            prb_tree_node* parentNode = nullptr;
            int direction = 0;
            prb_tree_node* currNode = *pRoot;
            while (currNode) {
                parentNode = currNode;
                direction = key_compare(*(keyT*)((char*)pNode + pKeyOffset), *(keyT*)((char*)currNode + pKeyOffset));
                if (direction != -1)
                    currNode = currNode->mChild[direction];
                else // Keys are equal
                    return currNode;
            }
            // Insert
            pNode->mChild[0] = pNode->mChild[1] = nullptr;
            pNode->set_parent(parentNode);
            pNode->set_red();
            parentNode->mChild[direction] = pNode;

            // Rebalance;
            prb_tree_node temp;
            temp.set_parent(nullptr);
            temp.mChild[1] = *pRoot;
            (*pRoot)->set_parent(&temp);

            currNode = parentNode;
            direction = currNode->mChild[1] == pNode;
            parentNode = currNode->parent();

            while (parentNode != nullptr) {
                int parentDirection = parentNode->mChild[1] == currNode;
                if (is_red(currNode->mChild[direction])) {
                    if (is_red(currNode->mChild[!direction])) {
                        currNode->set_red();
                        currNode->mChild[0]->set_black();
                        currNode->mChild[1]->set_black();
                    } else {
                        if (is_red(currNode->mChild[direction]->mChild[direction]))
                            parentNode->mChild[parentDirection] = rotate_once(currNode, !direction);
                        else if (is_red(currNode->mChild[direction]->mChild[!direction]))
                            parentNode->mChild[parentDirection] = rotate_twice(currNode, !direction);
                    }
                }
                currNode = parentNode; 
                direction = parentDirection;
                parentNode = currNode->parent();
            }
            *pRoot = temp.mChild[1];
            (*pRoot)->set_parent(nullptr);
        }


        // The root should be black
        (*pRoot)->set_black();
    
        return nullptr;
    }

    // Fast deletion of all elements
    template<prb_tree_node::EraseFunc erase_func>
    void prb_tree_node::erase(prb_tree_node *pRoot) 
    {
        prb_tree_node* parent = nullptr;
        prb_tree_node* currNode = pRoot;
        int direction = 0;
        // Go down to the leftmost without children
        while (currNode) {
            // Find smallest
            while (currNode->mChild[0]) {
                parent = currNode;
                direction = 0;
                currNode = currNode->mChild[0]; 
            }
            // If not a leaf, go right and repeat
            if (currNode->mChild[1]) {
                parent = currNode;
                direction = 1;
                currNode = currNode->mChild[1]; 
            } else {
                // currNode has no children.
                erase_func(currNode);
                // At this point we can't touch currNode as it may be dangling
                currNode = parent;

                if (parent) {
                    parent->mChild[direction] = nullptr;
                    parent = parent->parent();
                    if (parent)
                        direction = parent->mChild[1] == currNode;
                }
            }
        }
    }

    // returns in-order successor of pCurrNode, the smallest node if pCurrNode is nullptr
    prb_tree_node* prb_tree_node::next(prb_tree_node *pRoot, prb_tree_node* pCurrNode)
    {
        prb_tree_node* result = nullptr;
        if (pRoot) {
            if (!pCurrNode) {
                // get smallest
                result = pRoot;
                while (result->mChild[0])
                    result = result->mChild[0]; 
            } else {
                if (pCurrNode->mChild[1]) {
                    // larger child
                    result = pCurrNode->mChild[1];
                    // get the smallest in the subtree
                    while (result->mChild[0])
                        result = result->mChild[0]; 
                } else {
                    result = pCurrNode->parent();
                    while (result && result->mChild[1] == pCurrNode) {
                        // smaller parent. Go up while current node is the right child
                        pCurrNode = result;
                        result = pCurrNode->parent();
                    }
                }
            }
        }
        return result;
    }

    // Bottom-up deletion. returns nullptr if not found
    template<typename KeyT> 
    prb_tree_node* prb_tree_node::remove(prb_tree_node **pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset)
    {
        prb_tree_node* result = find<KeyT>(*pRoot, pKey, pKeyOffset);

        if(result) {
            remove(pRoot, result);
        }
        return result;
    }

    void prb_tree_node::remove(prb_tree_node **pRoot, prb_tree_node* pNode)
    {
        prb_tree_node* result = pNode;
        
        prb_tree_node tempRoot;
        tempRoot.set_parent(nullptr);
        tempRoot.mChild[1] = *pRoot;
        (*pRoot)->set_parent(&tempRoot);

            
        prb_tree_node* currNode = pNode;
        prb_tree_node* parentNode;
        prb_tree_node* childNode;
        prb_tree_node* tempNode;
        
        
        int direction;
        int direction2;

        //save Parent
        parentNode = currNode->parent();
        direction = parentNode->mChild[1] == currNode;

        if (currNode->mChild[0] != nullptr && currNode->mChild[1] != nullptr) {
            // the node has two children. find its in-order predecessor 
            currNode = currNode->mChild[0];
            direction2 = currNode->mChild[1] != nullptr;

            while (currNode->mChild[1] != nullptr)
                currNode = currNode->mChild[1];

            // move predecessor to result node
            if (direction2 == 0) {
                // predecessor is the left child of the result node

                // update result's parent's child
                parentNode->mChild[direction] = currNode;
                // update predecessor's parent.  predecessor's color now is result's.
                // result's color set to actually deleted node (predecessor)
                parentNode = currNode->mParent;
                currNode->mParent = result->mParent;
                result->mParent = parentNode;

                // update result's right child's parent
                result->mChild[1]->set_parent(currNode);
                // update predecessor's right child
                currNode->mChild[1] = result->mChild[1];

                // rebalance starts from CurrentNode. CurrentNode may have a left child 
                direction = 0;
            } else {
                // predecessor is the right child 

                // save parent
                tempNode = currNode->parent();
                // update predecessor's parent's right child.
                tempNode->mChild[1] = currNode->mChild[0];
                if (currNode->mChild[0])
                    currNode->mChild[0]->set_parent(tempNode);

                // replace result node with predecessor node
                            
                // update result's parent's child
                parentNode->mChild[direction] = currNode;
                // update predecessor's parent.  predecessor's color now is result's.
                // result's color set to actually deleted node (predecessor)
                parentNode = currNode->mParent;
                currNode->mParent = result->mParent;
                result->mParent = parentNode;

                // update result's right child's parent
                result->mChild[1]->set_parent(currNode);
                // update predecessor's right child
                currNode->mChild[1] = result->mChild[1];
                            
                // update result's left child's parent
                result->mChild[0]->set_parent(currNode);
                // update predecessor's left child
                currNode->mChild[0] = result->mChild[0];

                // rebalance starts from currNode. currNode may have a right child 
                currNode = tempNode;
                direction = 1; 
            }
        } else {
            // the node has less than two children
            childNode = currNode->mChild[currNode->mChild[0] == nullptr];
            // update parent's pointer
            parentNode->mChild[direction] = childNode;
            // update child's pointer 
            if (childNode)
                childNode->set_parent(parentNode);
                        
            // rebalance starts from currNode. currNode may have a child in [direction]
            currNode = parentNode;
        }


        // if deleted node is red, then there is no violation
        if (!result->is_red()) {
            // at this point result is the black deleted node. 
            // Start rebalancing from currNode. currNode may have a child in [direction] position
            childNode = currNode->mChild[direction];
            if (childNode && childNode->is_red()) {
                // child is red. fix violation by making child black
                childNode->set_black();
            } else { // rebalance
                // deleted node's parent
                parentNode = currNode; 
                prb_tree_node* siblingNode = parentNode->mChild[!direction]; // sibling of the deleted node
                
                prb_tree_node* grandParentNode = parentNode->parent();

                while (grandParentNode) {
                    direction2 = grandParentNode->mChild[1] == parentNode;

                    if (siblingNode && !siblingNode->is_red()) { // black sibling
                        if (!is_red(siblingNode->mChild[0]) && !is_red(siblingNode->mChild[1])) {
                            // node's siblings' children are black
                            // recolor sibling to red
                            siblingNode->set_red();
                            if (parentNode->is_red()) { // if parent is red, color it black and we are done
                                parentNode->set_black();
                                break;
                            }
                        } else {
                            // at least one of siblings' child is red
                            // if left - one rotation, if right - double rotation
                            // then recolor new parent with the old parent's color
                            // and color it's children black 
                            bool isParentRed = parentNode->is_red();

                            if (is_red(siblingNode->mChild[!direction]))
                                parentNode = rotate_once(parentNode, direction);
                            else
                                parentNode = rotate_twice(parentNode, direction);

                            grandParentNode->mChild[direction2] = parentNode;

                            if (isParentRed)
                                parentNode->set_red();
                            else 
                                parentNode->set_black();

                            parentNode->mChild[0]->set_black();
                            parentNode->mChild[1]->set_black();
                            break;
                        }
                    } else if (siblingNode->mChild[direction] != nullptr) { // red sibling with inner child
                        tempNode = siblingNode->mChild[direction];
                        if(!is_red(tempNode->mChild[0]) && !is_red(tempNode->mChild[1])) {
                            // sibling's children are black
                            parentNode = rotate_once(parentNode, direction);
                            parentNode->mChild[direction]->mChild[!direction]->set_red();
                        } else {
                            if (is_red(tempNode->mChild[direction])) //sibling's inner child is red
                                siblingNode->mChild[direction] = rotate_once(tempNode, !direction);

                            parentNode = rotate_twice(parentNode, direction);
                            
                            siblingNode->mChild[direction]->set_black();
                            parentNode->mChild[!direction]->set_red();
                        }
                        grandParentNode->mChild[direction2] = parentNode;

                        parentNode->set_black();
                        parentNode->mChild[direction]->set_black();

                        break;
                    }

                    //going up
                    direction = direction2;
                    parentNode = grandParentNode;
                    siblingNode = parentNode->mChild[!direction];

                    grandParentNode = parentNode->parent();
                } // end while
            }
        }

        //restore root
        *pRoot = tempRoot.mChild[1];
        
        //update root's color
        if (*pRoot != nullptr) {
            (*pRoot)->set_parent(nullptr);
            (*pRoot)->set_black();
        }
    }

    template<typename KeyT>
    prb_tree_node* prb_tree_node::find(prb_tree_node *pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset)
    {
        prb_tree_node* currNode = pRoot;
        int direction;
        while (currNode) {
            direction = key_compare(pKey, *(KeyT*)((char*)currNode + pKeyOffset));
            if (direction != -1)
                currNode = currNode->mChild[direction];
            else
                return currNode;
        }
        return nullptr;
    }

    template<typename KeyT>
    prb_tree_node* prb_tree_node::find_eq_or_greater(prb_tree_node *pRoot, const KeyT& pKey, ptrdiff_t pKeyOffset)
    {
        prb_tree_node* currNode = pRoot;
        prb_tree_node* result = nullptr;
        int direction;
        while (currNode) {
            direction = key_compare(pKey, *(KeyT*)((char*)currNode + pKeyOffset));
            if (direction == 0) {
                // currNode is larger
                result = currNode;
                currNode = currNode->mChild[0];
            } else if (direction == 1) {
                // currNode is smaller
                currNode = currNode->mChild[1];
            } else {
                // equal
                return currNode;
            }
        }
        return result;
    }
} // namespace p42

#endif // PRB_TREE_NODE_H