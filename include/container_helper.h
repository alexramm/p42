#ifndef CONTAINER_HELPER_H
#define CONTAINER_HELPER_H

#include <type_traits>

namespace p42
{   
    template <typename T>
    using EraseF = void (*)(T*) noexcept;

    // Element's erase callback. Called on each element on container cleanup.
    template <typename Func, typename Element>
    concept CanEraseElement = std::is_nothrow_invocable_r_v<void, Func, Element*>;

    // Example of above callback if Element was allocated using new operator
    template <typename Element>
    void delete_element(Element* pElement) noexcept
    {
        delete pElement;
    }

  // Standard layout check
    template <typename Element>
    concept StandardLayout = std::is_standard_layout_v<Element>;

    // Returns offset in bytes of a class member. The class needs to be of a standard layout type.
    // It casts to type, which prevents it to be a constexpr. Casting nullptr also is not quite legal, but it seems OK with most compilers
    #if defined __clang__
        #pragma clang diagnostic ignored "-Wnull-pointer-subtraction" 
    #endif
    template <typename T>
    // compiler's error output for constrains is hard to understand atm, use static_assert for now
    // requires StandardLayout<T>
    ptrdiff_t offset_of(auto T::*pMember)
    {
        static_assert(std::is_standard_layout_v<T>, "T must be a standard layout type");   
        return (char*)&((T*)nullptr->*pMember) - (char*)nullptr;
    }
    #if defined(__clang__)
        #pragma clang diagnostic push
        #pragma clang diagnostic warning "-Wnull-pointer-subtraction"
    #endif
    // Returns type of a class member
    template <typename T, typename M>
    M type_of(M T::*pMember);
    #if defined(__clang__)
        #pragma clang diagnostic pop
    #endif
 }// namespace p42

#endif// CONTAINER_HELPER_H
