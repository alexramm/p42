/////////////////////////////////////////////////////////////////////////////////////////
//
// Multiple-Producer Single-Consumer (MPSC) queue
// Serially processes submitted work items in the thread pool
// Work items are under no copy contract. Only one thread at a time has ownership of it. If any member of
// a workitem is shared between threads (not recommended), it should be threadsafe.
// async_serial_worker takes ownership of a workitem, process it async, then release it to the future, returned by scheduling function
// Returned future would not block if discarded by caller and workitem will be deleted by async_serial_worker after processing.
// Results of the processing (if needed) should be stored it the work item itself. if work_item's process function (operator()) throws,
// the exception will be accessible via future.

/////////////////////////////////////////////////////////////////////////////////////////
#ifndef MPSC_QUEUE_H
#define MPSC_QUEUE_H

#include <concepts>
#include <future>
#include <memory>
#include <queue>
#include <shared_mutex>

namespace p42
{
    template<typename Workitem>
    requires std::invocable<Workitem>
    class mpsc_queue
    {
    public:
        using WorkitemPtr = std::unique_ptr<Workitem>; // unique_ptr constructors are noexcept. we should not throw from under the lock. 
        using WorkitemFuture = std::future<WorkitemPtr>;

        // if pProcessQueueOnShutdown is true, all work items that are in the queue already must be processed
        mpsc_queue(bool pProcessQueueOnShutdown) : mProcessQueueOnShutdown(pProcessQueueOnShutdown) {}

        ~mpsc_queue() 
        {
            // at this point, no new schedule() calls should be made. This can't be really enforced from the inside of the class
            mShutdown.store(true, std::memory_order::release); // signal main thread to shutdown
            std::unique_lock lock(mShutdownLock); // would block until all current schedule() calls complete.
            
            try {
                mWorker.wait(); // wait for processing/canceling all work items in the queue
            } catch (...) {}
        }

        WorkitemFuture schedule(WorkitemPtr pWorkItem)
        {
            std::shared_lock shutdownLock(mShutdownLock, std::try_to_lock); //This lock blocks destructor until all schedule() calls acquire and release mWorkerMutex.
            if (!shutdownLock.owns_lock()) {
                // Shutdown in progress. We should never be here.
                throw std::runtime_error("Race! Instance's destructor already called from another thread.");
            }

            auto promise = std::promise<WorkitemPtr>();
            auto future = promise.get_future();
            std::scoped_lock lock(mWorkerMutex);

            mWorkItems.push(std::make_pair(std::move(pWorkItem), std::move(promise)));

            // if worker is not running, create a new one
            if (!mWorkerRunning) {
                // the previous mWorker may still be valid at this point, and may block for a short time here in it's destructor
                mWorker = std::async(std::launch::async, [this]() { this->async_func(); });
                mWorkerRunning = true;
            }
            return future;
        }

    private:
        void async_func() noexcept
        {
            WorkitemPtr workItem;
            std::promise<WorkitemPtr> promise;
            while (true) {
                // a work item extracted from the queue under the lock, but processed without it.
                {
                    std::scoped_lock lock(mWorkerMutex);
                    if (mWorkItems.empty()) { // nothing to do
                        mWorkerRunning = false;
                        break;
                    }
                    workItem = std::move(mWorkItems.front().first);
                    promise = std::move(mWorkItems.front().second);
                    mWorkItems.pop();
                }

                //process workitem in the current thread
                try {
                    if (mProcessQueueOnShutdown || !mShutdown.load(std::memory_order::acquire)) {
                        (*workItem.get())(); // processing
                        promise.set_value(std::move(workItem)); // returning results

                    } else {
                        throw std::runtime_error("Discarding workitem due to shutdown.");
                    }
                } catch (...) {
                    // if set_exception() or reset() throws, it's better to terminate
                    promise.set_exception(std::current_exception()); // returning exception
                    workItem.reset(); // deleting work item.
                }
            }
        };

        bool mProcessQueueOnShutdown;
        std::atomic<bool> mShutdown{ false };
        std::shared_mutex mShutdownLock; // ensures no other thread is waiting on mWorkerMutex before destructor blocks on mWorker
        std::mutex mWorkerMutex; // mWorkItems, mWorkerStopped, mWorker should be accessed only under this lock except in constructor and destructor 
        std::queue<std::pair<WorkitemPtr, std::promise<WorkitemPtr>>> mWorkItems;
        bool mWorkerRunning{ false }; // sets only by scheduling thread, resets by worker thread right before exit 
        std::future<void> mWorker;
    };

} // namespace p42
#endif // MPSC_QUEUE_H