/////////////////////////////////////////////////////////////////////////////////////////
// A tree element class must be of a standard layout type
// Example:
//     class element
//     {
//      public:
//         uint64_t        mKey;
//         prb_tree_node   mNode;
//         char            mOtherData[100];
//
//         data(uint64_t key) : mKey(key){}
//     };
//
//
// An instance of the prb_tree class for above element class can be defined as:
//
// prb_tree<&element::mNode, &element::mKey> data_map;
//
// A function of a template<typename T> void (*)(T*) noexcept type can be specified.
// It will be called on each element in the tree on ```prb_tree::erase()``` call
//
// I.e. p42::delete_element can be used to delete an element allocated using new()
// 
// prb_tree<&element::mNode, &element::mKey, delete_element<element>> data_map;
//
// By default, nothing is called per element on prb_tree::erase() call
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef PRB_TREE_H
#define PRB_TREE_H

#include <cstdint>
#include <type_traits>

#include "container_helper.h"
#include "prb_tree_node.h"

namespace p42
{
    // Element - class, containing prb_tree_node
    // node - pointer to the prb_tree_node member in class Element
    // key - pointer to a key member in class Element
    // erase_func - called on erase of an element if specified

    void do_nothing_with_element(void*) noexcept {};

    template <auto, auto, auto> class prb_tree_t; // helper to deduce types in prb_tree class template below
    
    template <typename Element, typename KeyT, prb_tree_node Element::*node, KeyT Element::*key, auto erase_func>
    // compiler's error output for constrains is hard to understand atm, use static_assert until it gets better
    // requires CanEraseElement<decltype(erase_func), Element>  && StandardLayout<Element>
    class prb_tree_t<node, key, erase_func> 
    {
    private:
        static_assert(std::is_nothrow_invocable_r_v<void, decltype(erase_func), Element*>, "Erase function should not throw");
        static_assert(std::is_standard_layout_v<Element>, "Tree element requires standard layout");

        // use static as it's impossible to make offset_of a constexpr as it requires casting
        static inline ptrdiff_t mNodeOffset = offset_of(node); // offset in bytes from the beginning of the Element to prb_tree_node member
        static inline ptrdiff_t mKeyOffset = offset_of(key) - mNodeOffset; // offset in bytes from the prb_tree_node member to KeyT member

        prb_tree_node *mRoot;

        // calculates element's pointer and invoke erase function. 
        // this would be passed to prb_tree_node::erase()
        static void node_erase_func(prb_tree_node* pElementNode) noexcept
        {
            erase_func((Element*)((char*)pElementNode - mNodeOffset));
        }

        Element* to_element(const prb_tree_node* treeNode)
        {
            return treeNode ? (Element*)((char*)treeNode - mNodeOffset) : nullptr;
        }

    public:
        prb_tree_t(): mRoot(nullptr) {}
        ~prb_tree_t()
        {
            erase();
        }

        // Returns nullptr if inserted or pointer to the existing node with the same key
        Element* insert(Element* pElement)
        {
            prb_tree_node* treeNode = prb_tree_node::insert<KeyT>(&mRoot, (prb_tree_node*)((char*)pElement + mNodeOffset), mKeyOffset);
            return treeNode ? (Element*)((char*)treeNode - mNodeOffset) : nullptr;
        }

        // returns nullptr if not found
        Element* remove(const KeyT& pKey)
        {
            prb_tree_node* treeNode = prb_tree_node::remove<KeyT>(&mRoot, pKey, mKeyOffset);
            return to_element(treeNode);
        }

        Element* find(const KeyT& pKey)
        {
            prb_tree_node* treeNode = prb_tree_node::find<KeyT>(mRoot, pKey, mKeyOffset);
             return to_element(treeNode);
        }
        Element* find_eq_or_greater(const KeyT& pKey)
        {
            prb_tree_node* treeNode = prb_tree_node::find_eq_or_greater<KeyT>(mRoot, pKey, mKeyOffset);
             return to_element(treeNode);
        }

        Element* next(Element* pElement = nullptr)
        {
            prb_tree_node* treeNode = pElement ? (prb_tree_node*)((char*)pElement + mNodeOffset) : nullptr;
            treeNode = prb_tree_node::next(mRoot, treeNode);
            return to_element(treeNode);
        }

        Element* get_root()
        {
            return to_element(mRoot);
        }

        //erase all elements
        Element* erase()
        { 
            if ((void*)erase_func != (void*)do_nothing_with_element)
                prb_tree_node::erase<node_erase_func>(mRoot); 
                
            mRoot = nullptr;
            return nullptr;
        }
    };

    // TODO: tThis and return in erase() above was added to go around Win clang's v16.0.5 error "definition with same mangled name .... do_nothing_with_element..." when default template params are used.
    // Check if fixed and rename prb_tree_t back to the prb_tree
    template <auto node, auto key, auto erase_func = do_nothing_with_element> class prb_tree: public prb_tree_t<node, key, erase_func>{}; // helper to deduce types in prb_tree class template below

} // namespace p42

#endif // PRB_TREE_H