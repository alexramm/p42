#include <gtest/gtest.h>
#include "mpsc_queue.h"

using namespace p42;
using namespace std::chrono;

struct work_item {
    int mData;
    work_item(int data = 0): mData(data) {}
    void operator() () {
        std::this_thread::sleep_for(100ms) ; 
        mData++; 
    }
};

TEST(async_serial_worker, schedule_and_process_workitem) {
    std::future<std::unique_ptr<work_item>> future;
    {
        mpsc_queue<work_item> worker(true);
        auto future2 = worker.schedule(std::make_unique<work_item>(1));
        std::this_thread::sleep_for(50ms);
        future = worker.schedule(std::make_unique<work_item>(2)); //completes on shutdown;
        ASSERT_EQ(future2.get()->mData, 2); 
    }
    ASSERT_EQ(future.get()->mData, 3); 
}

TEST(async_serial_worker, schedule_and_discard_workitem) {
    std::future<std::unique_ptr<work_item>> future;
    {
        mpsc_queue<work_item> worker(false);
        worker.schedule(std::make_unique<work_item>());
        std::this_thread::sleep_for(50ms); // ensure that first workitem in processing before schedule next one
        future = worker.schedule(std::make_unique<work_item>());
    }
    try {
        future.get();
    } catch (const std::exception& e) {
        ASSERT_STREQ(e.what(), "Discarding workitem due to shutdown.");
    }
}

TEST(async_serial_worker, work_item_throws) {
    auto worker = new mpsc_queue<work_item>(true);

    std::future<void> future;
    {
        worker->schedule(std::make_unique<work_item>());
        std::this_thread::sleep_for(25ms); // ensure that first workitem in processing before calling destructor
        future = std::async(std::launch::async, [&worker](){
            std::this_thread::sleep_for(25ms); // ensure that destructor in progress before scheduling new workitem
            worker->schedule(std::make_unique<work_item>());
        });
        delete worker; // this would block until first workitem is finished 
    }
    try {
        future.get();
    } catch (const std::exception& e) {
        ASSERT_STREQ(e.what(), "Race! Instance's destructor already called from another thread.");
    }
}

