//ChatGPT generated unit test. Almost there :) See comments at the end
#include <gtest/gtest.h>
#include "spsc_circular_queue.h"

using namespace p42;

TEST(spsc_circular_queue, push_pop) {
    spsc_circular_queue<int, 4> queue;

    // Test initial state
    EXPECT_TRUE(queue.is_empty());
    EXPECT_FALSE(queue.is_full());

    // Test push
    int a = 1;
    int b = 2;
    int c = 3;
    int d = 4;
    EXPECT_TRUE(queue.push(&a));
    EXPECT_TRUE(queue.push(&b));
    EXPECT_TRUE(queue.push(&c));
    EXPECT_TRUE(queue.push(&d));
    EXPECT_TRUE(queue.is_full());

    // Test pop
    EXPECT_EQ(queue.pop(), &a);
    EXPECT_FALSE(queue.is_empty());
    EXPECT_EQ(queue.pop(), &b);
    EXPECT_FALSE(queue.is_empty());
    EXPECT_EQ(queue.pop(), &c);
    EXPECT_FALSE(queue.is_empty());
    EXPECT_EQ(queue.pop(), &d);
    EXPECT_TRUE(queue.is_empty());
    EXPECT_FALSE(queue.is_full());

    // Test push and pop when queue is full
    EXPECT_TRUE(queue.push(&a));
    EXPECT_TRUE(queue.push(&b));
    EXPECT_TRUE(queue.push(&c));
    EXPECT_TRUE(queue.push(&d));
    EXPECT_TRUE(queue.is_full());
    EXPECT_FALSE(queue.push(&a));
    EXPECT_EQ(queue.pop(), &a);
    EXPECT_TRUE(queue.push(&a));
    EXPECT_EQ(queue.front(), &b);
    EXPECT_EQ(queue.pop(), &b);
    EXPECT_TRUE(queue.push(&b));
    EXPECT_EQ(queue.pop(), &c);
    EXPECT_EQ(queue.pop(), &d);
    EXPECT_EQ(queue.pop(), &a);
    //EXPECT_TRUE(queue.is_empty()); // ChatGPT made a mistake here
    EXPECT_FALSE(queue.is_empty());
    EXPECT_EQ(queue.pop(), &b);
    EXPECT_TRUE(queue.is_empty());

    //ChatGPT didn't cover few branches
    EXPECT_FALSE(queue.push(nullptr));
    EXPECT_EQ(queue.pop(), nullptr);
}