# Runs test, generates coverage report if the build type is "Coverage"
# Coverage builds requre proper compiler parameters set in the corresponding CXX_COVERAGE_OPTIONS,
# COVERAGE_REPORT_PATH, COVERAGE_TOOL, and RUNTOOL variables set in the CMakePreset.json

if(CMAKE_BUILD_TYPE STREQUAL "Coverage")
    if($ENV{COVERAGE_TOOL} STREQUAL "LLVM")
        get_filename_component(LCOV_TARGET ${TEST_EXECUTABLE} NAME)

        execute_process(COMMAND ${CMAKE_COMMAND} -E env LLVM_PROFILE_FILE=${LCOV_TARGET}.profraw ${TEST_EXECUTABLE} RESULT_VARIABLE TEST_RESULT)
        execute_process(COMMAND $ENV{RUNTOOL} llvm-profdata merge -sparse ${LCOV_TARGET}.profraw -o ${LCOV_TARGET}.profdata)
        execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory $ENV{COVERAGE_REPORT_PATH})
        execute_process(COMMAND $ENV{RUNTOOL} llvm-cov export ${TEST_EXECUTABLE} --instr-profile=${LCOV_TARGET}.profdata --format=lcov ${TEST_SOURCES}
            OUTPUT_FILE $ENV{COVERAGE_REPORT_PATH}/lcov.info)
        execute_process(COMMAND $ENV{RUNTOOL} llvm-cov report ${TEST_EXECUTABLE} --instr-profile=${LCOV_TARGET}.profdata ${TEST_SOURCES} OUTPUT_FILE $ENV{COVERAGE_REPORT_PATH}/lcov.txt) # reports to output
    else()
        message(FATAL_ERROR "Unsupported coverage tool: $ENV{COVERAGE_TOOL}")
    endif()
else()
    execute_process(COMMAND ${TEST_EXECUTABLE} RESULT_VARIABLE TEST_RESULT)
endif()

if(TEST_RESULT)
    message(FATAL_ERROR "Test failed")
endif()
