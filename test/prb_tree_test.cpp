#include <iterator>
#include <tuple>

#include <gtest/gtest.h>

#include "prb_tree.h"

using namespace p42;

//validates red black tree
template<typename keyT> 
int validate(prb_tree_node* pNode, prb_tree_node* pParent, ptrdiff_t pKeyOffset)
{
    if (pNode == nullptr)
        return 1; //black leaf

    if (pNode->parent() != pParent)
        return 0;

    // check keys
    if ((pNode->left() != nullptr && prb_tree_node::key_compare(*(keyT*)((char*)pNode->left() + pKeyOffset), *(keyT*)((char*)pNode + pKeyOffset)) != 0) ||
        ((pNode->right() != nullptr && prb_tree_node::key_compare(*(keyT*)((char*)pNode + pKeyOffset), *(keyT*)((char*)pNode->right() + pKeyOffset))) != 0))
        return 0;

    // check red rule violation: Both children of a red node are black
    if (prb_tree_node::is_red(pNode) && (prb_tree_node::is_red(pNode->left()) || prb_tree_node::is_red(pNode->right())))
        return 0;

    // get height for the both subtrees
    int leftHeight = validate<keyT>(pNode->left(), pNode, pKeyOffset);
    int rightHeight = validate<keyT>(pNode->right(), pNode, pKeyOffset);

    if (leftHeight == 0 || rightHeight == 0)
        return 0;

    // check black rule violation: Every simple path from a given node to any 
    // of its descendant leaves contains the same number of black nodes
    if (leftHeight != rightHeight)
        return 0;

    // increment height if black
    return pNode->is_red() ? rightHeight : rightHeight + 1;
}

struct element
{
    uint64_t  mKey;
    prb_tree_node mNode;
    char data[10];

    element(uint64_t pKey = 0) : mKey(pKey) {}
};

int validate(element* root)
{
    if (root == nullptr) return 1; // empty tree is one black leaf
    using KeyT = decltype(type_of(&element::mKey));
    size_t offset = offset_of(&element::mKey) - offset_of(&element::mNode); // offset from node to key
    return validate<KeyT>(&(root->mNode), nullptr, offset);
}


// unique keys set, hits all code paths
element rawData[] {0, 19, 3, 13, 44, 55, 15, 7, 22, 14, 5, 6, 8, 23, 4, 32, 9, 11, 20, 1, 2, 10, 12, 90, 91};
decltype(element::mKey) orderedKeys[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 20, 22, 23, 32, 44, 55, 90, 91};

TEST(prb_tree_test, insert_next_remove)
{
    prb_tree<&element::mNode, &element::mKey> tree;

    for (size_t i = 0; i < std::size(rawData); ++i) {
        // inserts unique keys. expect nullptr (inserted)
        auto res = tree.insert(rawData + i);
        ASSERT_EQ(res, nullptr);
        ASSERT_NE(validate(tree.get_root()), 0);
    }

    // check order
    element* ptr = nullptr;
    for (size_t i = 0; i < std::size(rawData); ++i) {
        ptr = tree.next(ptr);
        EXPECT_TRUE(ptr != nullptr);
        ASSERT_EQ(ptr->mKey, orderedKeys[i]);
    }

    // remove
    for (size_t i = 0; i < std::size(rawData); ++i) {
        ptr = tree.remove(rawData[i].mKey);
        EXPECT_TRUE(ptr != nullptr);
        ASSERT_EQ(ptr->mKey, rawData[i].mKey);
        ASSERT_NE(validate(tree.get_root()), 0);
    }
    // should be empty now
    EXPECT_TRUE(tree.next() == nullptr);
    EXPECT_TRUE(tree.get_root() == nullptr);

    do_nothing_with_element(nullptr); // this stub never gets called. call it here for code coverage report
}

TEST(prb_tree_test, find_remove_reinsert)
{
    prb_tree<&element::mNode, &element::mKey> tree;
    //insert
    for (size_t i = 0; i < std::size(rawData); ++i) {
        ASSERT_EQ(tree.insert(rawData + i), nullptr);
    }
    ASSERT_NE(validate(tree.get_root()), 0);

    //find, remove, and reinsert elements
    for (size_t i = 0; i < std::size(rawData); ++i) {
        auto& key = rawData[i].mKey;

        //remove
        element* removed = tree.remove(key);
        ASSERT_EQ(removed, &rawData[i]);
        ASSERT_NE(validate(tree.get_root()), 0);

        //must fail
        ASSERT_EQ(tree.remove(key), nullptr);
        ASSERT_EQ(tree.find(key), nullptr);

        auto inserted = tree.insert(removed);
        ASSERT_EQ(inserted, nullptr);
        //must fail
        ASSERT_EQ(tree.insert(removed), removed);
        ASSERT_NE(validate(tree.get_root()), 0);

        //must return reinserted pointer
        inserted = tree.find(key);
        ASSERT_EQ(inserted, removed);
    }

}

TEST(prb_tree_test, find_eq_or_greater)
{
    prb_tree<&element::mNode, &element::mKey> tree;
    //insert
    for (size_t i = 0; i < std::size(rawData); ++i) {
        tree.insert(rawData + i);
    }

    auto key = tree.next()->mKey;
    for (uint64_t i = 0; i < std::size(rawData); ++i) {

        auto eqNode = tree.find_eq_or_greater(key);
        auto grNode = tree.find_eq_or_greater(key + 1);
        ASSERT_EQ(eqNode->mKey, orderedKeys[i]);
        if (i < std::size(rawData) - 1) {
            ASSERT_EQ(grNode->mKey, orderedKeys[i + 1]);
            key = grNode->mKey;
        } else { //last element
            ASSERT_EQ(grNode, nullptr);
        }
    }
}

void on_erase(element* pElement) noexcept
{
    pElement->mKey = 0;
}

TEST(prb_tree_test, erase)
{
    prb_tree<&element::mNode, &element::mKey, on_erase> tree;

    uint32_t cycles = 1024 * 10;
    element* nodes = new element[cycles];

    for (uint64_t i = 0; i < cycles; ++i) {
        nodes[i].mKey = i + 1;
        // inserts unique keys
        ASSERT_EQ(tree.insert(nodes + i), nullptr);
    }

    tree.erase();

    for (uint64_t i = 0; i < cycles; ++i)
        ASSERT_EQ(nodes[i].mKey, 0);

    delete[] nodes;
}

TEST(prb_tree_test, remove)
{
    prb_tree<&element::mNode, &element::mKey, on_erase> tree;

    uint32_t cycles = 1;// 1024 * 10;
    element* nodes = new element[cycles];

    for (uint64_t i = 0; i < cycles; ++i) {
        nodes[i].mKey = i + 1;
        // inserts unique keys
        ASSERT_EQ(tree.insert(nodes + i), nullptr);
    }


    for (uint64_t i = 0; i< cycles; ++i)
        ASSERT_NE(tree.remove(i + 1), nullptr);

    delete[] nodes;
}