# p42 library

A collection of a templates I created in different times for a (very) narrow use cases. Some classes may not to the latest C++ standards, some uses experimental C++, build may brake on older compiler's versions, IntelliSense shows non-existing errors, etc.  
Container templates operate without memory allocations or call stack use (recursion) on insertion/deletion/iteration. The main use case is to cast an element class onto data in
memory (mapped or loaded from file, received from network, etc), then include it in a container in-place.
The idea is to keep container control structure as part of a tree/list/etc element. As a tradeoff, a type of such element's class has to be of a [standard layout](https://en.cppreference.com/w/cpp/language/classes#Standard-layout_class).
Some of the implementation goes to a gray area of undefined behavior in C++ standard, but seems to work with most (if not all) compilers and architectures. See comments in the code.

There is a map (RB-tree), D-list, and a combination of both (can store entries with duplicate keys and retrieve them in the order of insertion),
non-blocking circular queue (one thread can push, another pop without using any synchronization primitives).
ATM, only tree and non-blocking circular queue are updated and included here, other containers will be added later.
Usually, there are two parts - simple node class without any std dependencies, and a wrapper for convenience. For ex. prb_tree_node and prb_tree

Some comments in the source. Look into examples and UT to see how to use.

Here is how to [install, build, and test](./docs/build.md) this CMake project with presets.  
[Naming convention](./naming_convention.cpp) I found somewhere and decided to adopt in my projects.  

## spsc_circular_queue

Single producer single consumer circular queue. One thread (producer) writes pointers to buffer, another (consumer) reads that pointers without using any locks or atomics. Based on this
[whitepaper](https://scholar.colorado.edu/concern/reports/g732d980d).

[spsc_circular_queue](./include/spsc_circular_queue.h)

## prb_tree

Red-black binary tree with parent pointers

[prb_tree_node](./include/prb_tree_node.h) size is 3 pointers: parent, left child, right child.  
Node address must be aligned by 2 bytes as it uses LSB as red/black flag.  
node and sorting key must be members of a tree element class

Element class must be of a [standard layout](https://en.cppreference.com/w/cpp/language/classes#Standard-layout_class).

Example:

```C++
class element
{
public:
    uint64_t        mKey;
    prb_tree_node   mNode;
    char            mOtherData[100];

    data(uint64_t key) : mKey(key){}
};
```

prb_tre_node has no std:: dependencies and can be used directly if that a requirement. If not, [prb_tree](./include/prb_tree.h) wrapper template can be used for convenience

```C++
template <typename Element, typename KeyT, prb_tree_node Element::*node, KeyT Element::*key, auto erase_func>
    requires CanEraseElement<decltype(erase_func), Element>  && StandardLayout<Element>
    class prb_tree<node, key, erase_func> 
{
    ...
}
```

 An instance of the prb_tree class for above element class can be defined as:

```C++
prb_tree<&element::mNode, &element::mKey> map;
```

 A function of a ```template<typename T> void (*)(T*) noexcept``` type can be specified in template.
 It will be called on each element in the tree on ```prb_tree::erase()``` or destructor call
 I.e. delete_element function can be used to delete an element previously allocated using new()

```C++
template<typename Element>
void delete_element(Element* pElement) noexcept
{
    delete pElement;
}

prb_tree<&element::mNode, &element::mKey, delete_element<element>> map;
```

By default, nothing is called per element on prb_tree::erase() or destructor call

less then ```<``` operator should be defined for a key's type
