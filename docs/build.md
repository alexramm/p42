# CMake C++ template

CMake C++ project with UT. Supports MSVC, Clang for Windows, Clang, GCC for MacOS.
It uses Clang that comes with VS and XCode.
Check [C++ compiler support](https://en.cppreference.com/w/cpp/compiler_support)

## Install

### VSCode

- Install recommended extensions listed in .vscode/extension.json

### Windows

- Install [Build Tools for Visual Studio](https://visualstudio.microsoft.com/downloads/) or VS with ```C++ CMake tools for Windows``` and SDK that matches OS build
- Install Python (type ```python``` to install from store)
- For Clang,  install ```C++ Clang tools for Windows``` under ```Desktop development with C++`` optional components in VS installer

### MacOS

- Install Xcode Command Line Tools ```xcode-select --install```
- Install cmake v.3.21 or higher  ```brew install cmake```
- Install Ninja generator  ```brew install ninja```
- By default, gcc linked to Apple's Clang. Install gcc ```brew install gcc``` and use compiler name ```gcc-13``` etc in gcc preset. There are issues with the new ld in Xcode 15, so ```-ld_classic`` option is used for gcc

## Build

MSVC coverage report available in VS IDE. To create coverage report with Clang, select ```Clang Coverage``` preset. To change Clang's code coverage report format, modify [RunTest.cmake](./test/RunTest.cmake) file.  
Google test library may not compile in a Release build due to warnings as errors setting.
VSCode's CTest integration is a bit buggy atm if no test preset specified for a configuration.

### Build in VSCode

- On Windows, run VS Code from the Developer Command Prompt:

```shell
"%ProgramFiles%\Microsoft Visual Studio\2022\Professional\Common7\Tools\VsDevCmd.bat"
"%LocalAppData%\Programs\Microsoft VS Code\Code.exe"
```

or create shortcut to ```%comspec% /k ""%ProgramFiles%\Microsoft Visual Studio\2022\Professional\Common7\Tools\VsDevCmd.bat" && "%LocalAppData%\Programs\Microsoft VS Code\Code.exe""```

- Use CMake Tools extension's commands

### Command line

- Navigate to project folder
- In Windows, configure the environment: ```"%ProgramFiles%\Microsoft Visual Studio\2022\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x64```
- List all presets" ```cmake --list-presets=all```
- Configure preset " ```cmake --preset clang-win-debug```
- Build preset (add if needed): ```cmake --build --preset build-clang-win-debug``` or ```cmake --build <Folder from above command's output>```
- Clean and build: ```cmake --build --preset build-clang-win-debug --clean-first```

### Build in Visual Studio

On any unexplained build issue, try Project->Delete cache first.  
To filter out external source for code coverage, select ```test\CodeCoverage.runsettings``` file in ```Configure Run Settings```  
Code coverage reports may fail to open in IDE. The workaround is:

- Open ```Code Coverage Results``` window by adding its button to a toolbar.
- Use ```Import results``` button to see coverage report.
- Use ```Show Code Coverage Coloring``` button to highlight covered code in editor.
