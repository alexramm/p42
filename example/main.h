#ifndef MAIN_H
#define MAIN_H

#include <iostream>
#include <mutex>

// prb_tree_example.cpp
void profile_prb_tree_insert();
void prb_tree_tuple_key_element();
void prb_tree_array_key_element();
void prb_tree_multi_node_element();

// spsc_circular_queue_example.cpp
void non_blocking_messaging();

// mpsc_queue
void serial_processing(bool pProcessAll);

// snippets
void all_snippets();


// std::osyncstream is not supported on Apple clang atm
namespace p42
{
    class osyncstream : public std::ostream 
    {
    public:

        osyncstream(std::ostream& pOutStream = std::cout):
            std::ostream(pOutStream.rdbuf()), mLockGuard(get_mutex()) { }

    private:
        static std::mutex& get_mutex()
        {
            static std::mutex mutex;
            return mutex;
        }

        std::lock_guard<std::mutex> mLockGuard;
    };
}
#endif // MAIN_H