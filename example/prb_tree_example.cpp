#include <chrono>
#include <iostream>
#include <string_view>
#include <wchar.h>

#include "prb_tree.h"

using namespace p42;
using namespace std::string_view_literals;

struct element
{
    uint64_t  mKey;
    prb_tree_node mNode;

    element(uint64_t pKey = 0): mKey(pKey){}
};

void profile_prb_tree_insert()
{
    std::cout << "Profiling prb_tree::insert() ..." << std::endl;
    std::srand((uint32_t)std::time(nullptr)); // use current time to seed random generator

    uint64_t totalElements = 1000000;

    element* elements = new element[(uint32_t)totalElements];
    for (uint64_t i = 0; i < totalElements; ++i) {
        elements[i].mKey = std::rand();;
    }

    prb_tree<&element::mNode, &element::mKey> tree;
    auto start = std::chrono::high_resolution_clock::now();
    for (uint64_t i = 0; i < totalElements; ++i) {
        tree.insert(&elements[i]);
    }
    tree.erase();
    auto end = std::chrono::high_resolution_clock::now();
    auto time_span = duration_cast<std::chrono::duration<double>>(end - start);
    std::cout << "It took " << time_span.count() << " seconds to insert " << 
        totalElements << " elements" << std::endl;
    
    delete[] elements;
}

struct tuple_key_element
{
    prb_tree_node mNode;

    struct tuple_key
    {
        uint64_t mKeyPart1;
        uint64_t mKeyPart2;

        tuple_key(uint64_t pKeyPart1, uint64_t pKeyPart2) : mKeyPart1(pKeyPart1), mKeyPart2(pKeyPart2) {}
        friend bool operator<(const tuple_key &pKeyLeft, const tuple_key &pKeyRight)
        {
            return std::tie(pKeyLeft.mKeyPart1, pKeyLeft.mKeyPart2) < std::tie(pKeyRight.mKeyPart1, pKeyRight.mKeyPart2);
        }
    } mKey;

    tuple_key_element(uint64_t pKeyPart1, uint64_t pKeyPart2) : mKey(pKeyPart1, pKeyPart2) {}
};

void prb_tree_tuple_key_element()
{
    std::cout << std::endl << "Inserting elements with a compound key ..." << std::endl;
    tuple_key_element unsorted[] {{9, 5}, {9, 1}, {5, 5}, {4, 9}, {9, 9}};
    
    prb_tree<&tuple_key_element::mNode, &tuple_key_element::mKey> tree;

    std::cout << "Unsorted: ";
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        tree.insert(unsorted + i);
        std::cout << "[" << unsorted[i].mKey.mKeyPart1 << ", " << unsorted[i].mKey.mKeyPart2 << "] ";
    }
    std::cout << std::endl;

    std::cout << "Sorted:   ";
    tuple_key_element* ptr = nullptr;
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        ptr = tree.next(ptr);
        std::cout << "[" << ptr->mKey.mKeyPart1 << ", " << ptr->mKey.mKeyPart2 << "] ";
    }
    std::cout << std::endl;
}

struct array_key_element
{
    prb_tree_node mNode; 

    // std string can't be used as it makes array_key_element non standard layout type
    // https://en.cppreference.com/w/cpp/language/classes#Standard-layout_class
    struct array_key 
    {
        wchar_t str[64]{};

        friend bool operator<(const array_key &pKeyLeft, const array_key &pKeyRight)
        {
            return wcscmp(pKeyLeft.str, pKeyRight.str) < 0;
        }
        array_key(const std::wstring_view& pKey) { pKey.copy(str, std::size(str) - 1); }
    } mKey;

    array_key_element(const std::wstring_view& pKey) : mKey(pKey) {}
};

void prb_tree_array_key_element()
{
    std::cout << std::endl << "Inserting elements with an array key ..." << std::endl;
    array_key_element unsorted[] {L"cba"sv, L"abd"sv, L"abc"sv, L"bca"sv, L"123"sv};
    
    prb_tree<&array_key_element::mNode, &array_key_element::mKey> tree;

    std::cout << "Unsorted: ";
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        tree.insert(unsorted + i);
        std::wcout << "[" << unsorted[i].mKey.str << "] ";
    }
    std::cout << std::endl;

    std::cout << "Sorted:   ";
    array_key_element* ptr = nullptr;
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        ptr = tree.next(ptr);
        std::wcout << "[" << ptr->mKey.str << "] ";
    }
    std::cout << std::endl;
}

struct multi_node_element
{
    uint64_t  mKey;
    prb_tree_node mNode;
    uint64_t  mKey2;
    prb_tree_node mNode2;

    multi_node_element(uint64_t pKey = 0, uint64_t pKey2 = 0): mKey(pKey), mKey2(pKey2){}
};

void prb_tree_multi_node_element()
{
    std::cout << std::endl << "Inserting elements with multiple nodes per element ..." << std::endl;
    multi_node_element unsorted[] {{1, 9}, {2, 8}, {6, 5}, {5, 6}};
    
    prb_tree<&multi_node_element::mNode, &multi_node_element::mKey> tree;
    prb_tree<&multi_node_element::mNode2, &multi_node_element::mKey2> tree2;

    std::cout << "Unsorted: ";
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        tree.insert(unsorted + i);
        tree2.insert(unsorted + i);
        std::wcout << "[" << unsorted[i].mKey << ", " << unsorted[i].mKey2 << "] ";
    }
    std::cout << std::endl;

    std::cout << "Sorted by key 1:   ";
    multi_node_element* ptr = nullptr;
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        ptr = tree.next(ptr);
        std::wcout << "[" << ptr->mKey << ", " << ptr->mKey2 << "] ";
    }
    std::cout << std::endl;

    std::cout << "Sorted by key 2:   ";
    ptr = nullptr;
    for (size_t i = 0; i < std::size(unsorted); ++i) {
        ptr = tree2.next(ptr);
        std::wcout << "[" << ptr->mKey << ", " << ptr->mKey2 << "] ";
    }
    std::cout << std::endl;
}