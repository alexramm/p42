#include <ranges>
#include <string>
#include <vector>
//convert all to another type in one line.
void convert() 
{
 // MSVC only atm
 //std::vector<std::wstring> v{L"aa", L"bb"};
 //auto v2 = v | std::views::transform([](const auto& el) { return el.c_str(); }) | std::ranges::to<std::vector>();
}

// brace init array of pairs
consteval auto init_array()
{
    auto array = std::to_array<std::pair<std::wstring_view, std::string_view>>({
        { L"One", "One"},
        { L"Two", "Two" }
    });
    return array;
}

void all_snippets()
{
    convert();
    init_array();
}
