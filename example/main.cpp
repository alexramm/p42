#include "main.h"

int main()
{
    // snippets
    all_snippets();

    // prb_tree
    profile_prb_tree_insert();
    prb_tree_tuple_key_element();
    prb_tree_array_key_element();
    prb_tree_multi_node_element();

    // spsc_circular_queue
    non_blocking_messaging();

    // mpsc_queue
    serial_processing(true);
    serial_processing(false);
    return 0;
}