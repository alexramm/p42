#include "main.h"
#include "mpsc_queue.h"

using namespace p42;
using namespace std::chrono;

struct work_item 
{
    int mData;

    work_item(int data = 0): mData(data)
    {
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " " << mData << " work_item()\n";
    }

    ~work_item()
    {
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " " << mData << " ~work_item()\n";
    }

    void operator() ()
    {
        std::this_thread::sleep_for(1ms);
        if (mData < 0) {
            osyncstream(std::cout) << "#" << std::this_thread::get_id() << " " << mData << " operator() throwing\n";
            throw std::runtime_error(std::to_string(mData));
        }
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " " << mData << " operator()\n";
    }
};

void serial_processing(bool pProcessAll)
{
    osyncstream(std::cout) << "\n#" << std::this_thread::get_id() << " serial_processing() started. Process all items: " <<
        (pProcessAll? "true" : "false") << " \n";
    std::future<std::unique_ptr<work_item>> future, future2;
    std::future<void> futures[5];
    {
        mpsc_queue<work_item> worker(pProcessAll);
        for (int i = 0; i < 5; i++) {
            futures[i] = std::async(std::launch::async, [&worker, i]() { worker.schedule(std::make_unique<work_item>(i - 5)); } );
        }
        for (int i = 0; i < 5; i++) {
            try {
                futures[i].get();
            } catch(const std::exception& e) {
                osyncstream(std::cout) << "#" << std::this_thread::get_id() << " exception " << e.what() << "\n";
            }
        }
        future2 = worker.schedule(std::make_unique<work_item>(-10));
        worker.schedule(std::make_unique<work_item>(10));
        std::this_thread::sleep_for(10ms); 
        future = worker.schedule(std::make_unique<work_item>(11));
        worker.schedule(std::make_unique<work_item>(12));
        worker.schedule(std::make_unique<work_item>(-11));
    }
    osyncstream(std::cout) << "#" << std::this_thread::get_id() << " worker destroyed\n";

    try {
        auto res = future.get();
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " result " << res->mData << "\n";
        auto res2 = future2.get();
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " result " << res2->mData << "\n";

    } catch (const std::exception& e) {
        osyncstream(std::cout) << "#" << std::this_thread::get_id() << " exception " << e.what() << "\n";
    }
    osyncstream(std::cout) << "#" << std::this_thread::get_id() << "serial_processing() exit\n";  
}